/**
 *
 * @author Daniel Abraham
 */
public class IDEX {
    
    private int readReg1, readReg2, writeReg2016, writeReg1511, function, ALUSrc,
            ALUOp,regDst,memRead, memWrite,regWrite, memToReg;
    private short offset;
    
    public IDEX(){
        readReg1 = 0;
        readReg2 = 0;
        writeReg2016 = 0;
        writeReg1511 = 0;
        function = 0;
        ALUSrc = 0;
        ALUOp = 0;
        regDst = 0;
        memRead = 0; 
        memWrite = 0;
        regWrite = 0;
        memToReg = 0;
    }
    
    
    public int getReadReg1(){
        return readReg1;
    }
    
    public int getReadReg2(){
        return readReg2;
    }
    
    public short getOffset(){
        return offset;
    }
    
    public int getWriteReg2016(){
    return writeReg2016;
    }
    
    public int getWriteReg1511(){
    return writeReg1511;
    }
    
    public int getFunction(){
        return function;
    }
    
    public void setRegDst(int dest){
        regDst = dest;
    } 
    
    public int getRegDst(){
        return regDst;
    }
    
    public void setALUSrc(int ALUSrc){
        this.ALUSrc = ALUSrc;
    }
    
    public int getALUSrc(){
        return ALUSrc;
    }
    
    public int getALUOp(){
        return ALUOp;
    }
    
    public int getMemRead(){
        return memRead;
    }
    
    public int getMemWrite(){
        return memWrite;
    }
    
    public int getMemToReg(){
        return memToReg;
    }
    
    
    public int getRegWrite(){
        return regWrite;
    }
    
    public void setReadReg1(int reg1){
        readReg1 = reg1;
    }
    
    public void setReadReg2(int reg2){
        readReg2 = reg2;
    }
    
    public  void setOffset(short offset){
        this.offset = offset;
    }
    
    public void setWriteReg2016(int value){
        writeReg2016 = value;
    }
    
    public void setWriteReg1511(int value){
        writeReg1511 = value;
    }
    
    public void setFunction(int function){
        this.function = function;
    }
    
    public void setALUOp(int ALUOp){
        this.ALUOp = ALUOp;
    }
    
    public void setMemRead(int MemRead){
        this.memRead = MemRead;
    }
    
    public void setMemWrite(int MemWrite){
        this.memWrite = MemWrite;
    }
    
    public void setMemToReg(int MemToReg){
        this.memToReg = MemToReg;
    }
    
    public void setRegWrite(int regWrite){
        this.regWrite = regWrite;
    }
    public String toString() {
		return "Rreg1 = "+readReg1 +"| reg2 = "+ readReg2+"| 2016 = "+ writeReg2016 +
				" | 1511 = "+writeReg1511 + " | func = "+ function+" | alu src = "+ ALUSrc+
	            "\n aluop = "+ALUOp + " | regDst = "+regDst+" | memR "+memRead+" | memW = "+
				memWrite+" | Reg-write = "+regWrite+" | memToReg = "+ memToReg;
	}
    
}
