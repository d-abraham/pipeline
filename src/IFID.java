/**
 *
 * @author Daniel Abraham
 */
public class IFID {
    private int inst;
    
    public IFID(){
        inst = 0;
        
    }
    
    public int getInst(){
        return inst;
    }
    
    public void setInst(int inst){
        this.inst = inst;
    }
    
}
