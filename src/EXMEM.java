/**
 *
 * @author Daniel Abraham
 */
public class EXMEM {
    private int MemRead, MemWrite, MemToReg, RegWrite, ALUResult, WriteRegNum, SWValue;
    
    public EXMEM(){
        MemRead = 0;
        MemToReg = 0;
        MemWrite = 0;
        RegWrite = 0;
        ALUResult = 0;
        WriteRegNum = 0;
        SWValue = 0;
    }
    
    public int getALUResult(){
        return ALUResult;
    }
    
    public int getSWValue(){
        return SWValue;
    }
    
    public int getWriteRegNum(){
        return WriteRegNum;
    }
    
    public int getMemRead(){
        return MemRead;
    }
    
    public int getMemWrite(){
        return MemWrite;
    }
    
    public int getMemToReg(){
        return MemToReg;
    }
    
    public int getRegWrite(){
        return RegWrite;
    }
    
    public void setALUResult(int ALUresult){
        this.ALUResult = ALUresult; 
    }
    
    public void setSWValue(int value){
        SWValue = value;
    }
    
    public void setWriteRegNum(int RWReg){
        WriteRegNum = RWReg;
    }
    
    public void setMemRead(int MemRead){
        this.MemRead = MemRead;
    }
    
    public void  setMemWrite(int MemWrite){
        this.MemWrite = MemWrite;
    }
    
    public void setMemToReg(int MemToReg){
        this.MemToReg = MemToReg;
    }
    
    public void setRegWrite(int RegWrite){
        this.RegWrite = RegWrite;
    }
    //for testing
    public String tosString() {
		return "memRead = "+MemRead + " | memWrite = "+ MemWrite +" | memToReg = "+
    MemToReg +" | regWrite = " +RegWrite +" | aluResult = "+ ALUResult+" | wRegNum = "+
				WriteRegNum+"\n | swVal = "+ SWValue;
	}
    
}
