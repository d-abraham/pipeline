/**
 *
 * @author Daniel Abraham
 */
public class Pipeline {
    
    private Memory main_mem;    // main memory
    private Regs regs;          // 32 regs
    private IFID ifidWrite;     //Write register
    private IFID ifidRead;      //Read register
    private IDEX idexWrite;
    private IDEX idexRead;
    private EXMEM exmemWrite;
    private EXMEM exmemRead;
    private MemWB memwbWrite;
    private MemWB memwbRead;
    
    public Pipeline(){
    	main_mem = new Memory();
    	regs = new Regs();
        ifidWrite = new IFID();
        ifidRead = new IFID();
        idexWrite = new IDEX();
        idexRead = new IDEX();
        exmemWrite = new EXMEM();
        exmemRead = new EXMEM();
        memwbWrite = new MemWB();
        memwbRead = new MemWB();
    }
    
    //IF: fetch next instruction and put it in ifidwrite
    public void IF_stage(int inst){
        ifidWrite.setInst(inst);
    }
    //Read from ifidRead, decode and fetch then write to idexWrite
    public void ID_stage(){
        if(ifidRead.getInst() == 0){
            //set all idexWrite to 0
            idexWrite.setRegDst(0);
            idexWrite.setALUSrc(0);
            idexWrite.setALUOp(0);
            idexWrite.setMemRead(0);
            idexWrite.setMemWrite(0);
            idexWrite.setMemToReg(0);
            idexWrite.setRegWrite(0);
            idexWrite.setOffset((short)0);
            idexWrite.setReadReg1(0);
            idexWrite.setReadReg2(0);
            idexWrite.setWriteReg1511(0);
            idexWrite.setWriteReg2016(0);
            idexWrite.setFunction(0);
        }
        else{
            //op
            int opcode = ifidRead.getInst() >>> 26; 
            //offset
            idexWrite.setOffset((short) (ifidRead.getInst() & 0x0000FFFF));
            //reg1
            idexWrite.setReadReg1(regs.getReg((ifidRead.getInst() & 0x03E00000)>>>21));
            //reg2
            idexWrite.setReadReg2(regs.getReg((ifidRead.getInst() & 0x001F0000)>>>16));
            //reg1511&2016
            idexWrite.setWriteReg2016((ifidRead.getInst() & 0x001F0000)>>>16);
            idexWrite.setWriteReg1511((ifidRead.getInst() & 0x0000F800)>>>11);
            //function
            idexWrite.setFunction(ifidRead.getInst() & 0x0000003F);            
            //R-type
            if(opcode ==0){
            	idexWrite.setALUSrc(0);
                idexWrite.setALUOp(10);
                idexWrite.setRegDst(1);
                idexWrite.setMemRead(0);
                idexWrite.setMemWrite(0);
                idexWrite.setMemToReg(0);
                idexWrite.setRegWrite(1);
                idexWrite.setOffset((short)0);
            }
            else if(opcode == 0x20 || opcode == 0x23){    //lb
            	idexWrite.setALUSrc(1);
                idexWrite.setALUOp(0);
            	idexWrite.setRegDst(0);
                idexWrite.setMemRead(1);
                idexWrite.setMemWrite(0);
                idexWrite.setMemToReg(1);
                idexWrite.setRegWrite(1);
                idexWrite.setOffset((short)0);
                
            }
            else{     //sb
                idexWrite.setALUSrc(1);
                idexWrite.setALUOp(0);
                idexWrite.setMemRead(0);
                idexWrite.setMemWrite(1);
                idexWrite.setRegWrite(0);
                idexWrite.setRegDst(-1);
                idexWrite.setMemToReg(-1);
                idexWrite.setOffset((short)0);
            }
        }
    }
  
  //Perform instruction => read out of idexRead then write to exmemWrite
    public void EX_stage(){
    	//NOP
        if((idexRead.getRegDst() == 0) && (idexRead.getALUSrc() == 0) &&
                (idexRead.getALUOp()== 0) && (idexRead.getMemRead() == 0) &&
                (idexRead.getMemWrite() == 0) && (idexRead.getMemToReg() == 0) &&
                (idexRead.getRegWrite() == 0)){         
            exmemWrite.setMemWrite(0);
            exmemWrite.setMemRead(0);
            exmemWrite.setMemToReg(0);
            exmemWrite.setRegWrite(0);
        }
        else{
            //passing control signals
            exmemWrite.setMemWrite(idexRead.getMemWrite());
            exmemWrite.setMemRead(idexRead.getMemRead());
            exmemWrite.setMemToReg(idexRead.getMemToReg());
            exmemWrite.setRegWrite(idexRead.getRegWrite());
            //R-Type add and sub
            if((idexRead.getALUOp() == 10) && (idexRead.getALUSrc() == 0) && 
                    (idexRead.getRegDst() ==1)){                
                //sub
                if(idexRead.getFunction() == 0x22){
                    exmemWrite.setALUResult(idexRead.getReadReg1() - idexRead.getReadReg2());
                }
                //add
                else if(idexRead.getFunction() == 0x20){
                    exmemWrite.setALUResult(idexRead.getReadReg1() + 
                    		idexRead.getReadReg2());
                }
                exmemWrite.setSWValue(idexRead.getReadReg2());
                exmemWrite.setWriteRegNum(idexRead.getWriteReg1511());
            }
            //lb
            else if ((idexRead.getALUOp() == 0) && (idexRead.getALUSrc() == 1) && 
            		(idexRead.getRegDst() == 0)) {
            	exmemWrite.setALUResult(idexRead.getReadReg1() + idexRead.getOffset());     //ALU Res
            	exmemWrite.setWriteRegNum(idexRead.getWriteReg2016());                      //write Reg
            	exmemWrite.setSWValue(idexRead.getReadReg2());                              //SW value
            }
            //sb
            else if ((idexRead.getALUOp() == 0) && (idexRead.getALUSrc() == 1)) {
                exmemWrite.setALUResult(idexRead.getReadReg1() + idexRead.getOffset());
                exmemWrite.setSWValue(idexRead.getReadReg2());            
            }
            else{
                exmemWrite.setALUResult(0);
                exmemWrite.setSWValue(0);
                exmemWrite.setWriteRegNum(0);
            }
        }
    }
    
    //If lb, then the address in EX stage is index of main_mem, else pass info from
    //exmemRead to memwbWrite
    public void MEM_stage(){
        //nop
        if ((exmemRead.getMemRead() == 0) && (exmemRead.getMemWrite() == 0) && 
                (exmemRead.getMemToReg() == 0) && (exmemRead.getRegWrite() == 0)) {
        	memwbWrite.setWriteRegNum(0);
        	memwbWrite.setLwDataValue(0);   
        	memwbWrite.setRegWrite(0);
            memwbWrite.setMemToReg(0);
            memwbWrite.setAluResult(0);
        }
        else {
        	//copying needed info 
            memwbWrite.setRegWrite(exmemRead.getRegWrite());
            memwbWrite.setMemToReg(exmemRead.getMemToReg());
            memwbWrite.setAluResult(exmemRead.getALUResult());
          //R-type
            if ((exmemRead.getMemRead() == 0) && (exmemRead.getMemWrite() == 0)) {
            	memwbWrite.setWriteRegNum(exmemRead.getWriteRegNum());
    		}
          //lb
            else if ((exmemRead.getMemRead() == 1)&&(exmemRead.getMemWrite() == 0)) {
    			memwbWrite.setLwDataValue(main_mem.getMainMemAddress(
    					exmemRead.getALUResult()));
    			memwbWrite.setWriteRegNum(exmemRead.getWriteRegNum());
    		}
            //sb
            else if ((exmemRead.getMemRead() == 0)&&(exmemRead.getMemWrite() == 1)) {
    			main_mem.setMainMem(exmemRead.getALUResult(), (short) exmemRead.getSWValue());
    		}
            
		}
      }

  //Read from memwbRead and write it in regs
    public void WB_stage() {
    	//add,sub and lb
		if(memwbRead.getRegWrite() == 1) {
			//add or sub
			if(memwbRead.getMemToReg() == 0) {
				regs.setReg(memwbRead.getWriteRegNum(), memwbRead.getAluResult());
			}
			else {
				//lb
				regs.setReg(memwbRead.getWriteRegNum(), memwbRead.getLwDataValue());
			}
		}
	}
  
    public String print_out_everything() {
		StringBuilder report = new StringBuilder("Registers:\n");
		//printing regs
		report.append(regs.toString()+"\n\n");
		//IFID
		report.append("IF/ID Write \n Inst: 0x"+ Integer.toHexString(ifidWrite.getInst())+"\n");
		report.append("IF/ID Read \n Inst: 0x"+ Integer.toHexString(ifidRead.getInst())+"\n\n");
		//IDEX-W
		report.append("ID/EX Write \n RegDst= " + idexWrite.getRegDst()+"\n");
		report.append("ALUSrc= " + idexWrite.getALUSrc()+"\n");
		report.append("ALUOp= " + idexWrite.getALUOp()+"\n");
		report.append("MemRead= " + idexWrite.getMemRead()+"\n");
		report.append("MemWrite= " + idexWrite.getMemWrite()+"\n");
		report.append("MemToReg= " + idexWrite.getMemToReg()+"\n");
		report.append("RegWrite= " + idexWrite.getRegWrite()+"\n");
		report.append("ReadReg1Value= " + Integer.toHexString(idexWrite.getReadReg1())+"\n");
		report.append("ReadReg2Value= " + Integer.toHexString(idexWrite.getReadReg2())+"\n");
		report.append("SEOffset= 0x" + Integer.toHexString( idexWrite.getOffset())+"\n");
		report.append("WriteReg_20_16= " + idexWrite.getWriteReg2016()+"\n");
		report.append("WriteReg_15_11= " + idexWrite.getWriteReg1511()+"\n");
		report.append("Function= 0x" + Integer.toHexString( idexWrite.getFunction() )+"\n\n");
		//IDEX-R
		report.append("ID/EX Read \n RegDst= " + idexRead.getRegDst()+"\n");
		report.append("ALUSrc= " + idexRead.getALUSrc()+"\n");
		report.append("ALUOp= " + idexRead.getALUOp()+"\n");
		report.append("MemRead= " + idexRead.getMemRead()+"\n");
		report.append("MemWrite= " + idexRead.getMemWrite()+"\n");
		report.append("MemToReg= " + idexRead.getMemToReg()+"\n");
		report.append("RegWrite= " + idexRead.getRegWrite()+"\n");
		report.append("ReadReg1Value= " + Integer.toHexString(idexRead.getReadReg1())+"\n");
		report.append("ReadReg2Value= " + Integer.toHexString(idexRead.getReadReg2())+"\n");
		report.append("SEOffset= 0x" + Integer.toHexString( idexRead.getOffset())+"\n");
		report.append("WriteReg_20_16= " + idexRead.getWriteReg2016()+"\n");
		report.append("WriteReg_15_11= " + idexRead.getWriteReg1511()+"\n");
		report.append("Function= 0x" + Integer.toHexString( idexRead.getFunction() )+"\n\n");
		//EXMEM-W
		report.append("EX/MEM Write \n MemRead= " + exmemWrite.getMemRead()+"\n");
		report.append("MemWrite= " + exmemWrite.getMemWrite()+"\n");
		report.append("MemToReg= " + exmemWrite.getMemToReg()+"\n");
		report.append("RegWrite= " + exmemWrite.getRegWrite()+"\n");
		report.append("ALUResult= " + Integer.toHexString(exmemWrite.getALUResult())+"\n");
		report.append("SWValue= 0x" + Integer.toHexString( exmemWrite.getSWValue())+"\n");
		report.append("WriteRegNum= " + exmemWrite.getWriteRegNum()+"\n\n");
		//EXMEM-R
		report.append("EX/MEM Read \n MemRead= " + exmemRead.getMemRead()+"\n");
		report.append("MemWrite= " + exmemRead.getMemWrite()+"\n");
		report.append("MemToReg= " + exmemRead.getMemToReg()+"\n");
		report.append("RegWrite= " + exmemRead.getRegWrite()+"\n");
		report.append("ALUResult= " + Integer.toHexString(exmemRead.getALUResult())+"\n");
		report.append("SWValue= 0x" + Integer.toHexString( exmemRead.getSWValue())+"\n");
		report.append("WriteRegNum= " + exmemRead.getWriteRegNum()+"\n\n");
		//MEMWB-W
		report.append("MEM/WB Write \n MemToReg= " + memwbWrite.getMemToReg()+"\n");
		report.append("RegWrite= " + memwbWrite.getRegWrite()+"\n");
		report.append("LWDataValue= " + memwbWrite.getLwDataValue()+"\n");
		report.append("ALUResult= " + Integer.toHexString(memwbWrite.getAluResult())+"\n");
		report.append("WriteRegNum= " + memwbWrite.getWriteRegNum()+"\n\n");
		//MEMWB-R
		report.append("MEM/WB Read \n MemToReg= " + memwbRead.getMemToReg()+"\n");
		report.append("RegWrite= " + memwbRead.getRegWrite()+"\n");
		report.append("LWDataValue= " + memwbRead.getLwDataValue()+"\n");
		report.append("ALUResult= " + Integer.toHexString(memwbRead.getAluResult())+"\n");
		report.append("WriteRegNum= " + memwbRead.getWriteRegNum()+"\n\n");
		return report.toString();
	}
    
  
    public void copy_write_to_read() {
    	//IFID-W >> IFID-R
    	ifidRead.setInst(ifidWrite.getInst());
    	
    	//IDEX-W >> IDEX-R
    	idexRead.setRegDst(idexWrite.getRegDst());
    	idexRead.setALUOp(idexWrite.getALUOp());
    	idexRead.setALUSrc(idexWrite.getALUSrc());
    	idexRead.setRegWrite(idexWrite.getRegWrite());
    	idexRead.setMemWrite(idexWrite.getMemWrite());
    	idexRead.setMemRead(idexWrite.getMemRead());
    	idexRead.setMemToReg(idexWrite.getMemToReg());
    	idexRead.setOffset(idexWrite.getOffset());
    	idexRead.setWriteReg1511(idexWrite.getWriteReg1511());
    	idexRead.setWriteReg2016(idexWrite.getWriteReg2016());
    	idexRead.setFunction(idexWrite.getFunction());
    	idexRead.setReadReg1(idexWrite.getReadReg1());
    	idexRead.setReadReg2(idexWrite.getReadReg2());
    	
    	//EXMEM-W >> EXMEM-R
    	exmemRead.setALUResult(exmemWrite.getALUResult());
    	exmemRead.setSWValue(exmemWrite.getSWValue());
    	exmemRead.setWriteRegNum(exmemWrite.getWriteRegNum());
    	exmemRead.setMemRead(exmemWrite.getMemRead());
    	exmemRead.setMemWrite(exmemWrite.getMemWrite());
    	exmemRead.setRegWrite(exmemWrite.getRegWrite());
    	exmemRead.setMemToReg(exmemWrite.getMemToReg());
    	
    	//MEMWB-W >> MEMWB-R
    	memwbRead.setAluResult(memwbWrite.getAluResult());
    	memwbRead.setWriteRegNum(memwbWrite.getWriteRegNum());
    	memwbRead.setMemToReg(memwbWrite.getMemToReg());
    	memwbRead.setRegWrite(memwbWrite.getRegWrite());
    	memwbRead.setLwDataValue(memwbWrite.getLwDataValue());
	}
    //for testing
    public String toString() {
		return regs.toString() +"\n"+ main_mem.toSting();
	}
 }

