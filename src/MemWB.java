/**
 *
 * @author Daniel Abraham
 */
public class MemWB {
    
    private int memToReg, regWrite, lwDataValue, aluResult, writeRegNum;
    
    public MemWB(){
        memToReg = 0;
        regWrite = 0;
        lwDataValue = 0;
        aluResult = 0;
        writeRegNum = 0;
    }
    
    public void setLwDataValue(int lwDataValue){
        this.lwDataValue = lwDataValue;
    }
    
    public int getLwDataValue(){
        return lwDataValue;
    }
    
    public void setAluResult(int aluResult){    // to be used with exmem
        this.aluResult = aluResult;
    }
    
    public int getAluResult(){
        return aluResult;
    }
    
    public void setWriteRegNum(int writeRegNum){    // to be used with exmem
        this.writeRegNum = writeRegNum;
    }
    
    public int getWriteRegNum(){
        return writeRegNum;
    }
    
    public void setMemToReg(int memToReg){
        this.memToReg = memToReg;
    }
    
    public int getMemToReg(){
        return memToReg;
    }
    
    public void setRegWrite(int regWrite){
        this.regWrite = regWrite;
    }
    
    public int getRegWrite(){
        return regWrite;
    }
    //testing
    public String tosString() {
		return "memToR="+memToReg+ " regW="+ regWrite + " lwDataV="+ lwDataValue +
				" aluRes="+aluResult + " wRegNum=" + writeRegNum;
	}
    
}
