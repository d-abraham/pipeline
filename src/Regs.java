/**
 *
 * @author Daniel Abraham
 */
public class Regs {
    private int [] regs;

    public Regs(){
        regs = new int[32];
        initCache();
    }

    //initialize regs
    private void initCache(){
    	regs[0] = 0;
        for (int i=1; i < 32; i++){
            regs[i] = i+0x100;
        }
    }
    
    public int getReg(int num){
        return regs[num];
    }
    
    public void setReg(int regNum, int value){
        regs[regNum] = value;
    }

    public String toString(){
        StringBuilder display = new StringBuilder();
        int newline = 0;
        for (int x=0; x<32; x++){
            display.append("$"+x+" = 0x"+Integer.toHexString(regs[x])+"\t");
            newline++;
            if (newline >= 4) {
				display.append("\n");
				newline = 0;
			}
        }
        return display.toString();
    }

}
