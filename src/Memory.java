/**
 *
 * @author Daniel Abraham
 */
public class Memory {
    private short[] mainMem;

    public Memory(){
        mainMem = new short[1024];
        initMainMem();
    }

    //Initialize MainMem
    private void initMainMem(){
        short value = 0;
        for (int x=0; x<1024; x++){
            if (value >0xff){
                value =0;
            }
            mainMem[x] = value;
            value++;
        }
    }

    public void setMainMem(int address, Short data){
        mainMem[address] = data;
    }

    public int getMainMemAddress(int address){
        return mainMem[address];
    }

    public String toSting(){
        StringBuilder x = new StringBuilder("Address start | Offsets into data \n");
        int value = 0;
        for (int pos=0; pos<1024; pos++){
            if (value > 0xf){
                value = 0;
                x.append("\n");
            }
            if (value == 0){
                x.append(Integer.toHexString(pos)+" | ");
            }
            x.append(Integer.toHexString(mainMem[pos])).append("   ");
            value++;


        }
        return x.toString();
    }
    
}
