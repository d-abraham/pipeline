public class Driver {

	public static void main(String[] args) {
		int[] list = {0xa1020000,0x810AFFFC,0x00831820,0x01263820,0x01224820,0x81180000,
    			0x81510010,0x00624022,0x00000000,0x00000000,0x00000000,0x00000000};
		Pipeline pipeline = new Pipeline();
    	int cycleNum = 1;
    	for (int inst = 0; inst < list.length; inst++) {
    		System.out.println("Clock cycle: "+cycleNum);
    		pipeline.IF_stage(list[inst]);
    		pipeline.ID_stage();
    		pipeline.EX_stage();
    		pipeline.MEM_stage();
    		pipeline.WB_stage();		
    		System.out.println(pipeline.print_out_everything());
    		pipeline.copy_write_to_read();
			cycleNum++;
		}
    	System.out.println(pipeline.toString());

	}

}
